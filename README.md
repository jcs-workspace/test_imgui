[![C++ Standard](https://img.shields.io/badge/standard-17-black.svg?style=flat&logo=cplusplus)]()
[![Vulkan](https://img.shields.io/badge/vulkan-1.3.243.0-9C1C20.svg?style=flat&logo=vulkan)](https://www.vulkan.org/)

# test_imgui

Test project to use [ImGui](https://github.com/ocornut/imgui) from scratch. (CMake)

![demo](/etc/demo.png)

## 🔨 How to use?

```sh
# clone this repo
git clone https://gitlab.com/jcs-workspace/test_imgui --recurse

# navigate to project dir
cd test_imgui

# Build CMake project
start scripts/build.bat
```
