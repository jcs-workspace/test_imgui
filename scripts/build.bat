@echo off
:: ========================================================================
:: $File: build.bat $
:: $Date: 2023-04-02 21:41:56 $
:: $Revision: $
:: $Creator: Jen-Chieh Shen $
:: $Notice: See LICENSE.txt for modification and distribution information
::                   Copyright © 2023 by Shen, Jen-Chieh $
:: ========================================================================

cd ../

cmake -S . -B build/windows/
